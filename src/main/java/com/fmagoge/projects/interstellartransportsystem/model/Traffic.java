package com.fmagoge.projects.interstellartransportsystem.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
public class Traffic {
	@Id
	private int routeId;
	private String planetOrigin;
	private String planetDestination;
	private Double trafficDelay;
	public Traffic() {
	}
	public int getRouteId() {
		return routeId;
	}
	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}
	public String getPlanetOrigin() {
		return planetOrigin;
	}
	public void setPlanetOrigin(String planetOrigin) {
		this.planetOrigin = planetOrigin;
	}
	public String getPlanetDestination() {
		return planetDestination;
	}
	public void setPlanetDestination(String planetDestination) {
		this.planetDestination = planetDestination;
	}
	public Double getTrafficDelay() {
		return trafficDelay;
	}
	public void setTrafficDelay(Double trafficDelay) {
		this.trafficDelay = trafficDelay;
	}
	
	
	
	
}
