package com.fmagoge.projects.interstellartransportsystem.model;

import javax.persistence.Entity;
import javax.persistence.Id;
@Entity
public class Routes {
	
	@Id
	private int routeId;
	private String planetOrigin;
	private String planetDestination;
	private Double distance;
	public Routes() {
	}
	public int getRouteId() {
		return routeId;
	}
	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}
	public String getPlanetOrigin() {
		return planetOrigin;
	}
	public void setPlanetOrigin(String planetOrigin) {
		this.planetOrigin = planetOrigin;
	}
	public String getPlanetDestination() {
		return planetDestination;
	}
	public void setPlanetDestination(String planetDestination) {
		this.planetDestination = planetDestination;
	}
	public Double getDistance() {
		return distance;
	}
	public void setDistance(Double distance) {
		this.distance = distance;
	}
	
	
	
	
}
