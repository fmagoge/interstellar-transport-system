package com.fmagoge.projects.interstellartransportsystem;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.fmagoge.projects.interstellartransportsystem.model.PlanetNames;


public class ReadDataFromExcel {
	private static final String EXCEL_FILE_LOCATION = "C:\\temp\\Worksheet in C  Users Administrator Downloads HR-Offsite AssignmentV3 0.xlsx";
	
	@Autowired
	  private EntityManagerFactory entityManagerFactory;

	 
	  
	  public SessionFactory getSessionFactory() {
	    SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);

	    if (sessionFactory == null) {
	      throw new NullPointerException("factory is not a hibernate factory");
	    } else {
	      return sessionFactory;
	    }
	  }
	public void readData() throws IOException {
		SessionFactory sf = entityManagerFactory.unwrap(SessionFactory.class);
		Session session = sf.openSession();
		FileInputStream file = new FileInputStream(new File(EXCEL_FILE_LOCATION)); 
		XSSFWorkbook workbook = new XSSFWorkbook(file); 
		XSSFSheet sheet = workbook.getSheetAt(1); 
        Row row;
        for(int i=1; i<=sheet.getLastRowNum(); i++){  //points to the starting of excel i.e excel first row
            row = (Row) sheet.getRow(i);  //sheet number
            
            
	            int id;
				if( row.getCell(0)==null) { id = 0; }
	            else id= i;

                String node;
				if( row.getCell(1)==null) { node = "null";}  //suppose excel cell is empty then its set to 0 the variable
                   else node = row.getCell(1).toString();   //else copies cell data to name variable

                String name;
				if( row.getCell(2)==null) { name = "null";   }
                   else  name   = row.getCell(2).toString();
		Transaction t = session.beginTransaction();
	    PlanetNames planetNames = new PlanetNames();
	    planetNames.setId(id);
	    planetNames.setName(name);
	    planetNames.setNode(node);
	    System.out.println(planetNames.getId()+" "+planetNames.getName()+" "+planetNames.getNode());
	    session.saveOrUpdate(planetNames);
	    t.commit();		
        }
		file.close();
	}
}
