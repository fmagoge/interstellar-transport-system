package com.fmagoge.projects.interstellartransportsystem.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fmagoge.projects.interstellartransportsystem.model.PlanetNames;
import com.fmagoge.projects.interstellartransportsystem.repository.PlanetNamesRepository;


@RestController
public class PlanetNamesController {

	@Autowired(required=true)
	PlanetNamesRepository planetNamesRepository;
	
	@RequestMapping(value="/travel",method=RequestMethod.GET)
	public String newTravel(ModelMap model) {
		PlanetNames planet = new PlanetNames();
		model.addAttribute("planet",planet);
		return "traveldetails.html";
	}
	
	@RequestMapping(value="/save",method=RequestMethod.POST)
	public String saveRegistration(@Valid PlanetNames planet,BindingResult result,ModelMap model,RedirectAttributes redirectAttributes) {
		
		if(result.hasErrors()) {
			System.out.println("has errors");
			return "traveldetails.html";
		}
	
		planetNamesRepository.save(planet);
		
		return "redirect:/viewpath";
	}
	
	
	
}
