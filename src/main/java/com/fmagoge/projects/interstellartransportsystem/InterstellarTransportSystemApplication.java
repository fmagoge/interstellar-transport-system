package com.fmagoge.projects.interstellartransportsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InterstellarTransportSystemApplication {

	public static void main(String[] args) {
		SpringApplication.run(InterstellarTransportSystemApplication.class, args);
		
		try {
			ReadDataFromExcel readDataFromExcel = new ReadDataFromExcel();
			readDataFromExcel.readData();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
