package com.fmagoge.projects.interstellartransportsystem.services;



import java.util.List;

import com.fmagoge.projects.interstellartransportsystem.model.PlanetNames;

public interface PlannetNameService {

	List<PlanetNames> listAll();

	PlanetNames getById(Long id);

	PlanetNames saveOrUpdate(PlanetNames planetNames);

    void delete(Long id);

    PlanetNames saveOrUpdatePlanetOrNamesForm(PlanetNames productForm);
}
