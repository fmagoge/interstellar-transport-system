package com.fmagoge.projects.interstellartransportsystem.repository;

import org.springframework.data.repository.CrudRepository;

import com.fmagoge.projects.interstellartransportsystem.model.PlanetNames;

public interface PlanetNamesRepository extends CrudRepository<PlanetNames, Integer> {

	PlanetNames findPlanetByName(String name);
	

}
