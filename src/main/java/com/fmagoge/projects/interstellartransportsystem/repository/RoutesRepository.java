package com.fmagoge.projects.interstellartransportsystem.repository;

import org.springframework.data.repository.CrudRepository;

import com.fmagoge.projects.interstellartransportsystem.model.Routes;

public interface RoutesRepository extends CrudRepository<Routes, Integer> {

}
