package com.fmagoge.projects.interstellartransportsystem.repository;

import org.springframework.data.repository.CrudRepository;

import com.fmagoge.projects.interstellartransportsystem.model.Traffic;

public interface TrafficRepository extends CrudRepository<Traffic, Integer> {

}
